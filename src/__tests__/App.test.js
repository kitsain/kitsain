import React from 'react';
import { shallow } from 'enzyme'
import App from '../App';
import Result from '../Result'
import Scanner from '../Scanner'

describe('App', () => {
let wrapper;
beforeEach(() => {
  wrapper = shallow(<App/>);
  jest.clearAllMocks();
  
});

describe('_scan function', () =>{

  test('function changes states correctly', () => {
    expect(wrapper.state('scanning')).toBe(false)
    expect(wrapper.state('status')).toBe('Waiting')
    expect(wrapper.state('result')).toBe('')

    wrapper.instance()._scan()

    expect(wrapper.state('scanning')).toBe(true)
    expect(wrapper.state('status')).toBe('Scanning')
    expect(wrapper.state('result')).toBe('')
  });
});

describe('_onDetected function',() => {
  test('function changes states correctly',()=>{

    wrapper.instance()._scan()

    expect(wrapper.state('scanning')).toBe(true)
    expect(wrapper.state('status')).toBe('Scanning')
    expect(wrapper.state('result')).toBe('')

    wrapper.instance()._onDetected('Testituote')

    expect(wrapper.state('scanning')).toBe(false)
    expect(wrapper.state('status')).toBe('Scanned')
    expect(wrapper.state('result')).toBe('Testituote')
  });
});

describe('getDate function',() => {
  test('should change state when given date', () => {
    expect(wrapper.state('date')).toBe('')

    wrapper.instance().getDate('1.1.2021')

    expect(wrapper.state('date')).toBe('1.1.2021')
  })
});


describe('Render function', () => {

test('should render scan button', () => {
  const button = wrapper.find('#scanbutton');
  expect(wrapper.state('scanning')).toBe(false);
  expect(button).toHaveLength(1);
  expect(button.text()).toEqual('Scan');
});

test('should change scan button to stop when scanning true', () => {
  wrapper.setState({scanning: true});
  const button = wrapper.find('#scanbutton');
  expect(wrapper.state('scanning')).toBe(true);
  expect(button).toHaveLength(1);
  expect(button.text()).toEqual('Stop');
});

test('scan button should call _scan function',() => {
  const spy = jest.spyOn(wrapper.instance(), '_scan');
  wrapper.instance().forceUpdate()
  wrapper.find('#scanbutton').simulate('click')
  expect(spy).toHaveBeenLastCalledWith()
});
test('should render different component based on scanning state',() => {
  expect(wrapper.state('scanning')).toBe(false);
  expect(wrapper.find(Result)).toHaveLength(1)

  wrapper.setState({scanning: true})

  expect(wrapper.find(Scanner)).toHaveLength(1)
})
});
});

//describe('name',() => {});
//test('name',() => {})