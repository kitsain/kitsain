import React, { useState } from 'react';
import { shallow, mount } from 'enzyme'
import Result from '../Result';
import DateChooser from '../DateChooser'

const initStates = (gotProductState, failScrapeState, scrapingState, productNameState, errorState) => {
    // Init wanted states IN ORDER THEY ARE IN THE COMPONENT and use them with spyon.mockImplementationOnce
    let gotProduct = gotProductState
    let failScrape = failScrapeState
    let scraping = scrapingState
    let productName = productNameState
    let error = errorState
    jest.spyOn(React, 'useState')
    .mockImplementationOnce(() => useState(gotProduct))
    .mockImplementationOnce(() => useState(failScrape))
    .mockImplementationOnce(() => useState(scraping))
    .mockImplementationOnce(() => useState(productName))
    .mockImplementationOnce(() => useState(error))
}



describe('Result', () => {
    let wrapper;

    let resultMock = JSON.parse('{"codeResult": { "code": "20010416"}}')
    beforeEach(() => {
      //wrapper = shallow(<Result/>);
      jest.clearAllMocks();
    });
    describe('Render function',() => {
        test('should render scraping text when scraping state is true',() => {
            initStates(false,false,true,'','')
            wrapper = shallow(<Result result={''}/>);
            expect(wrapper.find('#scraping')).toHaveLength(1)
            expect(wrapper.find('#scraping').text()).toEqual('Scraping...')
            expect(wrapper.find('#EAN')).toHaveLength(0)
        });
        test('should render EAN given on props and Datechooser component when gotProduct is true and failScrape and scraping is false',() => {
            initStates(true,false,false,'','')
            wrapper = shallow(<Result result={resultMock}/>);
            expect(wrapper.find('#scraping')).toHaveLength(0)
            expect(wrapper.find('#EAN')).toHaveLength(1)
            expect(wrapper.find('#EAN').text()).toEqual('Scanned EAN: ' + resultMock.codeResult.code)
            expect(wrapper.find(DateChooser).exists()).toEqual(true)     
        });
        test('Should render EAN given on props, Datechooser component and error message when gotProduct and failScrape is true',() => {
            initStates(true,true,false,'','Tuotetta ei löytynyt!')
            wrapper = shallow(<Result result={resultMock}/>);
            expect(wrapper.find('#scraping')).toHaveLength(0)
            expect(wrapper.find('#EAN')).toHaveLength(1)
            expect(wrapper.find('#EAN').text()).toEqual('Scanned EAN: ' + resultMock.codeResult.code)
            expect(wrapper.find('#error')).toHaveLength(1)
            expect(wrapper.find('#error').text()).toEqual('Tuotetta ei löytynyt!')
            expect(wrapper.find(DateChooser).exists()).toEqual(true)
        });
    });

});

//describe('name',() => {});
//test('name',() => {});