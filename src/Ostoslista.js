import React, { Component } from 'react'
import { FaEdit, FaCheck } from "react-icons/fa";
import { Droppable, Draggable } from 'react-beautiful-dnd';

const grid = 8;
const bgColor = (time) => {
    // console.log(time)

    if (new Date() - time > 43200000)
        return "#ff3333"
    else if (new Date() - time < -144000000)
        return "#33ffad"
    else if (time === undefined)
        return "lightgrey"
    else
        return "#ffff80"
}
const getItemStyle = (isDragging, draggableStyle, time) => ({
    // some basic styles to make the items look a bit nicer
    userSelect: 'none',
    padding: 1,
    margin: `0 0 ${grid}px 0`,

    //  display:list === 1 ? "true" : "none",
    // change background colour if dragging

    background: isDragging ? 'lightgreen' : bgColor(time),

    // styles we need to apply on draggables
    ...draggableStyle
});

// const hideShow = (isDraggingover) =>
// ({
//     background: isDraggingover ? 'red' : 'lightgrey',
//     padding: 1,
//     color: "black",
//     width: "11em",
//     minHeight: 20,
//     overflow: "hidden"
// })

const getListStyle = isDraggingOver => ({
    background: isDraggingOver ? 'lightblue' : 'rgba(186, 186, 186,0.5)',
    padding: 1,
    width: "10.5em",
    color: "black",
    minHeight: 20,
    overflow: "auto",
    maxHeight: "12em",
    height: "11.5em",
    overflowX: "hidden"
});


export default class Ostoslista extends Component {
    constructor(props) {
        super(props);

        this.state = {
            checkedItems: [],
            checked: [],
            ostoskori: this.props.ostoskori,
        };


    }

    componentDidUpdate() {
     
        // if (this.state.booleanList.length !== this.props.ostoskori.length) {
        //     this.updateBooleanList()
        // }

        //tän takia checkbox nollaantuu jos lisää uuden tuotteen
        if (this.state.ostoskori!== this.props.ostoskori) {

            this.setState({ ostoskori: this.props.ostoskori })
        }


    }

    handleCheckboxChange = (index) => {
        // console.log(this.props.ostoskori)
        let listaCopy = [...this.state.ostoskori]
        let item = {
            ...listaCopy[index],
            checked: !this.state.ostoskori[index].checked

        }
        listaCopy[index] = item;
        this.setState({ ostoskori: listaCopy })
        //     let listaCopy = [...this.state.booleanList]
        //tallennetaan uusi data local storageen
       this.props.changeList(undefined,undefined,undefined,listaCopy)
    };

//funktio jolla siirretään tuotteet ostoslistaan
    HandleClick = () => {


        let checkedItem = [];
        let notChecked = []
        console.log(this.state.ostoskori)
        //jos tuotteen checkked on true niin lisätään checked listaan, muuten lisätään not checked listaan
        // this.state.ostoskori.map((value, index) => {
        //     if (value.checked === true) {
        //         checkedItem = [...checkedItem, this.state.ostoskori[index]]
        //         this.state.ostoskori[index].checked = false
        //     }
        //     else
        //         notChecked = [...notChecked, this.state.ostoskori[index]]

        // })
        
      this.state.ostoskori.forEach((value,index) => {
        if (value.checked === true) {
            checkedItem = [...checkedItem, this.state.ostoskori[index]]
            this.state.ostoskori[index].checked = false
        }
        else
            notChecked = [...notChecked, this.state.ostoskori[index]]
      });
        console.log(checkedItem)

        this.props.addToShopList(checkedItem, notChecked)
       

        // this.setState({ checkedItems: [], booleanList: [] })

    }


    render() {
        return (
            <div>
                <h style={{ alignItems:"left", backgroundColor:"grey", fontSize:12, 
                      cursor:"pointer",  boxShadow: "1px 2px 1px #757575", paddingRight:5,paddingLeft:5, paddingTop:2,paddingBottom:2,
                      paddingRight:2, marginBottom:2, borderRadius:6, borderStyle: "solid",
                       borderWidth: 1, fontWeight:"bold", color: "white", display: "inline-block" }} onClick={() => this.HandleClick(this.state.checkedItems)}>lisää valitut tuotelistaan</h>
                <Droppable droppableId="droppable2">

                    {(provided, snapshot) => (
                        <div
                        className={this.props.itemstyle}
                            ref={provided.innerRef}
                            style={getListStyle(snapshot.isDraggingOver)}>
                            {this.props.ostoskori.map((item, index) => (
                                <Draggable
                                    key={index}
                                    draggableId={item.tuote + index + "O"}
                                    index={index}>
                                    {(provided, snapshot) => (
                                        <div
                                            ref={provided.innerRef}
                                            {...provided.draggableProps}
                                            {...provided.dragHandleProps}
                                            style={getItemStyle(
                                                snapshot.isDragging,
                                                provided.draggableProps.style
                                            )}>
                                            <div style={{ minHeight: "2.5em", position: "relative" }}> {this.props.editProduct === index + "O" ?
                                                <div>
                                                    <input className="editInput" placeholder={this.props.ostoskori[index].tuote} value={this.props.editValue} onChange={(value) => this.props.editItem(value, index)}></input>
                                                    <input className="editInput" placeholder={this.props.ostoskori[index].EAN} value={this.props.editEanValue} onChange={(value) => this.props.editEan(value, index)}></input>
                                                </div>
                                                :
                                                item.tuote === "" ? <div style={{ width: "84%", overflow: "hidden" }}>{item.EAN} <input className="radioButton" type="checkbox" checked={this.state.ostoskori[index] === undefined ? false : this.state.ostoskori[index].checked} onChange={(e) => this.handleCheckboxChange(index)}></input> </div>  :
                                                    <div style={{ width: "84%", overflow: "hidden" }}><h style={{ marginLeft: 20, textDecorationLine: 'underline' }}>
                                                        <input className="radioButton" type="checkbox" checked={this.state.ostoskori[index] === undefined ? false : this.state.ostoskori[index].checked} onChange={(e) => this.handleCheckboxChange(index)}></input> {item.tuote} </h></div>}
                                                {this.props.editButton ? <FaEdit className="editIcon" onClick={() => this.props.allowEdit(index, "O")} size="20px"></FaEdit> :
                                                    this.props.editProduct === index + "O" ? <FaCheck className="editIcon" onClick={() => this.props.confirmEdit(index, "O")} size="20px"></FaCheck> : null}
                                            </div>

                                        </div>
                                    )}
                                </Draggable>
                            ))}
                            {provided.placeholder}
                        </div>
                    )}
                </Droppable>
            </div>
        )
    }
}
