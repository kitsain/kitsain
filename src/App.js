import React, { Component } from 'react';
import Scanner from './Scanner';
import Result from './Result.js';
import { BiRefresh } from "react-icons/bi";
import {clearCache} from "clear-cache"
import 'react-datepicker/dist/react-datepicker.css'
import './App.css';

class App extends Component {

  state = {
    // State used if the camera is scanning
    scanning: false,
    // State used to tell the user what the scanner is doing
    status: 'Waiting',
    // State to hold the scanned result
    result: '',
    // State to hold the date received from Datechooser component
    date: '',
  }

  // Method for changing states when scanner is active
  _scan = () => {
    this.setState({ scanning: !this.state.scanning, status: this.state.scanning ? 'Waiting' : 'Scanning', result: ''});
  }

  // Method for changing states when scan has detected the EAN
  _onDetected = async (result) => {
    let parsedresult;
    parsedresult = await this.parseResult(result);
    this.setState({
      result: parsedresult,
      scanning: false,
      status: 'Scanned',
    });
  }

  // Parses the result to be just one variable with the EAN
  parseResult = (result) => {
    let EAN;
    try {
      EAN = result.codeResult.code
      return EAN
    }
      catch (e){
      return result
    }
  }
  
  // Used to trigger manual search
  manualResult = (result) => {
    this._scan();
    this._onDetected(result)
  }

    componentWillMount() {
    //console.log(window.location.search.substr(1).valueOf())

    //http://localhost:3000/?tuote=piim%C3%A4,2019-08-25
    const queryString = window.location.search;

    const urlParams = new URLSearchParams(queryString);
    //console.log(urlParams);
    const page_type = urlParams.getAll('tuote')

    const itemss = localStorage.getItem("items");
    const itemss2 = localStorage.getItem("waste");
   // console.log(itemss)
    if (itemss == null)
    {
      localStorage.setItem("items", "[]");
    }
  //  console.log(itemss2)
    if (itemss2 == null)
    {
      localStorage.setItem("waste", "[]");
    }


    console.log(page_type);
    this.setState({param_array: page_type})

    for(let i = 0; i < page_type.length; i++)
    {
      var str = page_type[i];
      var res = str.split(",");
      this.additem(res[0], res[1])

    }
    
  } 
    
  additem (item,date) {
    console.log("tuote ja bestbefore:" + item,date)
    let items = '';
    //  const items_list = [];
    //console.log("og date:" + date)
    let newDate = Date.parse(date);
   // console.log("pvm:" + newDate)
    let dateobj = new Date(newDate);
    let dateitem = JSON.stringify(dateobj);
    const newItem = { "tuote": item, "bb": dateitem}

    if(new Date() - newDate > 43200000)
    {
      items = localStorage.getItem("waste");
    }
    else if (new Date() - newDate < -144000000)
    {
      items = localStorage.getItem("items");
    }
    
    //console.log(JSON.parse(items))
    let items_list = JSON.parse(items)
    items_list = [...items_list, newItem]
    console.log("DATE CALC:" + " " + (new Date() - newDate))
    console.log("ITEM_LIST" + items_list)
    if(new Date() - newDate > 43200000)
    {
      localStorage.setItem("waste", JSON.stringify(items_list));
    }
    else if (new Date() - newDate < -144000000)
    {
      localStorage.setItem("items", JSON.stringify(items_list));
    }
    else
    {

     // localStorage.setItem("items", JSON.stringify(items_list));
    }

   // localStorage.setItem("items", JSON.stringify(items_list));
   // const items_list = localStorage.getItem("items");
   // console.log(items_list);
  }

  // Mock method for testing
  mock = () => {
    this._scan();
    
    
  
    // 6410405055767 pirkka oregano
    // 6415712000813 s-puuro
    // 7310050001692 löft kahvi
    // 6410405088666 pirkka jliha
    // 6414893500075 s-ryhmä jauheliha
    // 20010416 Marsalkka kahvi lidl
    // 4056489114710 Schwarzwälder kinkku lidl
  
    let result = JSON.parse('{"codeResult": { "code": "20010416"}}');
    this._onDetected(result)
    
  }

  // Callback function from Result component to get the date
  getDate = (date) => {
    this.setState({date: date})
  }

  hardRefresh = () => {
    try {
      clearCache()
    } catch (error) {
      console.log('Page refresh error: ',error)
    }
    
  }

  render() {
    //console.log(this.state.result)
    return (
      
      <div style={{flexDirection:"column", backgroundColor:"rgba(100,100,100,0.4)", width:"100%", height:"100vh"}}>
        <div>
          <button  id='scanbutton' onClick={this._scan}>{this.state.scanning ? 'Stop' : 'Scan'}</button>
          {/* <button  id='mockbutton' onClick={this.mock}>Mock</button> */}
          <button style={{width:'10%',height:'10%'}}  id='refresh' onClick={this.hardRefresh}><BiRefresh/></button>
       
             {/* </div> */}
        </div>
          
          {/* turha div? <div> */}
        
      {this.state.scanning ? <div id='scanner'> <Scanner onDetected={this._onDetected}/></div> : null}
      <div style={{ display: (this.state.scanning ? 'none' : 'block')}} id='result'>
      <Result result={this.state.result} sendDate={this.getDate} sendResult={this.manualResult} />
      </div>
      </div>
    )
  }

}

export default App;
