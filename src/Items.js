import React, { Component } from 'react'
//import AddToCalendar from 'react-add-to-calendar';
import DatePicker from "react-datepicker";
import CalendarContainer from "react-datepicker";
import ReactAddToCalendar from "./ReactAddToCalendar"
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import { FaTrashAlt, FaEdit, FaCheck, } from "react-icons/fa";
import "./App.css"
import Ostoslista from "./Ostoslista"
/* import Viideslista from "./Viideslista" */
const reorder = (list, startIndex, endIndex) => {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);

    return result;
};

/**
* Moves an item from one list to another list.
*/


const move = (source, destination, droppableSource, droppableDestination) => {
    const sourceClone = Array.from(source);
    const destClone = Array.from(destination);
    const [removed] = sourceClone.splice(droppableSource.index, 1);

    destClone.splice(droppableDestination.index, 0, removed);

    const result = {};
    result[droppableSource.droppableId] = sourceClone;
    result[droppableDestination.droppableId] = destClone;
    console.log(result)
    return result;
};

// const getColor = (x) => {
// if(x===100)
// return("grey")
// }
const grid = 8;
const bgColor = (time) => {
    // console.log(time)

    if (new Date() - time > 43200000)
        return "#ff3333"
    else if (new Date() - time < -144000000)
        return "#33ffad"
    else if (time === undefined)
        return "lightgrey"
    else
        return "#ffff80"
}


const getItemStyle = (isDragging, draggableStyle, time) => ({
    // some basic styles to make the items look a bit nicer
    userSelect: 'none',

    margin: `0 0 ${grid}px 0`,

    //  display:list === 1 ? "true" : "none",
    // change background colour if dragging

    background: isDragging ? 'lightgreen' : bgColor(time),

    // styles we need to apply on draggables
    ...draggableStyle
});

const hideShow = (isDraggingover) =>
({
    background: isDraggingover ? 'rgba(232, 39, 39,0.6)' : 'rgba(186, 186, 186,0.5)',
    padding: 1,
    color: "black",
   // width: "10em",
    minHeight: 20,
    overflow: "auto",
    maxHeight: "12em",
    height:"11.5em",
    overflowX: "hidden",
})

const getListStyle = isDraggingOver => ({
    background: isDraggingOver ? 'lightblue' : 'rgba(186, 186, 186,0.5)',
    padding: 1,
    //width: "10em",
    color: "black",
    height:"11.5em",
    minHeight: 20,
    overflow: "auto",
    maxHeight: "12em",
    overflowX: "hidden",
    // overflowY:"scroll"
   
});


export default class ItemList extends Component {
    constructor(props) {
        super(props);
        // Don't call this.setState() here!
        this.state = {
            value: "",
            is_open: true,
            items: this.props.tuotelista,
            ostoskori: this.props.ostosKori,
            used: this.props.used,
            waste: this.props.wasted,
            viides: this.props.viideskori,
            is_open: true,
            roskakori: [],
            display_roskakori: "none",
            display_items: '',
            display_waste: 'none',
            display_used: 'none',
            display_ostoskori: 'none',
            display_viides: 'none',
            history: true,
            editProduct: "",
            editValue: "",
            editEan:"",
            editButton: true,
            //lista state määrittää mille listalle tuote lisätään, oletuksena T eli tuotelista
            lista: "T",
        };

    }

    id2List = {
        droppable: 'items',
        droppable2: 'ostoskori',
        droppable3: 'used',
        droppable4: 'waste',
        droppable5: 'roskakori',
        droppable6: 'viideskori',
    };
    componentDidUpdate() {
        if (this.state.items !== this.props.tuotelista || this.state.ostoskori !== this.props.ostosKori || this.state.used !== this.props.used || this.state.waste !== this.props.wasted || this.state.viides !== this.props.viides)
            this.setState({ items: this.props.tuotelista, ostoskori: this.props.ostosKori, used: this.props.used, waste: this.props.wasted, viides: this.props.viides })
    }
    getList = id => this.state[this.id2List[id]];

    onDragEnd = result => {
        const { source, destination } = result;
        console.log("dragend")
        this.setState({ showIcon: false })
        // dropped outside the list
        if (!destination) {
            return;
        }
        //console.log(source.droppableId + "destiid" + destination.droppableId)
        if (source.droppableId === destination.droppableId) {
            const items = reorder(
                this.getList(source.droppableId),
                source.index,
                destination.index
            );

            let state = { items };

            if (source.droppableId === 'droppable2') {
                state = { ostoskori: items };
            }
            else if (source.droppableId === 'droppable3') {
                state = { used: items };
            }
            else if (source.droppableId === 'droppable4') {
                state = { waste: items };
            }
            else if (source.droppableId === 'droppable6') {
                state = { viides: items };
            }
            this.setState(state);
            console.log("changepos")
            this.props.changeData(items, source.droppableId)
        } else {
            const result = move(
                this.getList(source.droppableId),
                this.getList(destination.droppableId),
                source,
                destination
            );
            this.props.changeList(source.droppableId, destination.droppableId, result.droppable, result.droppable2, result.droppable3, result.droppable4, result.droppable6)


        }
    };
    addToShopList = (checkedItems,notChecked) => {
     
          console.log(checkedItems)
          const combinedArray = [...this.state.items,...checkedItems]
        //   this.setState({items:combinedArray},() => 
       this.props.changeList(undefined, undefined, combinedArray,notChecked)
        //    )
        //   const items = this.state.items.concat(checkedItems)
        //   this.setState({items:items})
          console.log(this.state.items)
        //   localStorage.setItem("items", JSON.stringify(this.state.items))
      
        }

    editItem = (e) => {
        // this.state.items[index].tuote = "kana";
        console.log(e.target.value)
        this.setState({ editValue: e.target.value })

    }

    editTime = (time) => {
       
    
        time += "T10:30:00.000Z"
// console.log(jotain.setHours(12))
// jotain = jotain.setHours(12,30,0,0)
//console.log(Intl.DateTimeFormat(['ban', 'id']).format(jotain.setHours(12,30,0,0)))

return JSON.stringify(time)
    }

    editEan = (e) => {
        // this.state.items[index].tuote = "kana";
        console.log(e.target.value)
        this.setState({editEan: e.target.value })

    }
    confirmEdit = (index, lista) => {
console.log(this.state.editValue)
console.log(this.state.time)
        //Tuotelistan muokkaus
        if ((this.state.editValue !== "" || this.state.editEan !=="" || this.state.time !== undefined)  && lista === "T") {
            // this.state.items[index].tuote = this.state.editValue;
           
            let listaCopy = [...this.state.items]
            let item = {
                ...listaCopy[index],
                tuote: this.state.editValue !== "" ? this.state.editValue: this.state.items[index].tuote,
                EAN:this.state.editEan !== "" ? this.state.editEan: this.state.items[index].EAN,
                bb: this.state.time !== undefined? this.editTime(this.state.time) : this.state.items[index].bb
                
                
            }
            listaCopy[index] = item;
            this.setState({ listaCopy })
            this.props.changeList(undefined, undefined, listaCopy)
            //this.setState({ editButton: true, editProduct: "", editValue: "" })
        }
        //ostoslistan muokkaus
        else if ((this.state.editValue !== "" || this.state.editEan !=="") && lista === "O") {
            let listaCopy = [...this.state.ostoskori]
            let item = {
                ...listaCopy[index],
                tuote: this.state.editValue !== "" ? this.state.editValue: this.state.ostoskori[index].tuote,
                EAN: this.state.editEan !== "" ? this.state.editEan: this.state.ostoskori[index].EAN
            }
            listaCopy[index] = item;
            this.setState({listaCopy })
            this.props.changeList(undefined, undefined, undefined, listaCopy)
            //this.setState({ editButton: true, editProduct: "", editValue: "" })
        }
        else if ((this.state.editValue !== "" || this.state.editEan !=="") && lista === "H") {
            let listaCopy = [...this.state.waste]
            let item = {
                ...listaCopy[index],
                tuote: this.state.editValue !== "" ? this.state.editValue: this.state.waste[index].tuote,
                EAN: this.state.editEan !== "" ? this.state.editEan: this.state.waste[index].EAN
            }
            listaCopy[index] = item;
            this.setState({ listaCopy })
            this.props.changeList(undefined, undefined, undefined, undefined, undefined, listaCopy)
           // this.setState({ editButton: true, editProduct: "", editValue: "" })
        }
        else if ((this.state.editValue !== "" || this.state.editEan !=="") && lista === "R") {
            console.log(this.state.used[index].tuote)
            let listaCopy = [...this.state.used]
            let item = {
                ...listaCopy[index],
                tuote: this.state.editValue !== "" ? this.state.editValue: this.state.used[index].tuote,
                EAN: this.state.editEan !== "" ? this.state.editEan: this.state.used[index].EAN
            }
            listaCopy[index] = item;
            this.setState({ listaCopy })
            
            this.props.changeList(1, undefined, undefined, undefined, listaCopy, undefined)
            //this.setState({ editButton: true, editProduct: "", editValue:"",editEan:"" })
        }
        //jos input tyhjä niin säilytetään tuotenimi
        else {
            this.setState({ editButton: true, editProduct: "", editValue: "",editEan:"" })
        }

        this.setState({ editButton: true, editProduct: "", editValue:"",editEan:"" })
    }
    allowEdit = (index, list) => {
        console.log("edit" + index + list)
        this.setState({ editProduct: index + list, editButton: false })
    }




    // }
    hideShow = (e) => {
        console.log(e.draggableId.slice(-1))
        if (e.draggableId.slice(-1) !== "E")
            this.setState({ display_roskakori: "", showIcon: true })


    }
    // handleDateChange = (index,date) => {
    //     let listaCopy = [...this.state.items]
    //         let item = {
    //             ...listaCopy[index],
    //             tuote: this.state.editValue !== "" ? this.state.editValue: this.state.items[index].tuote,
    //             EAN:this.state.editEan !== "" ? this.state.editEan: this.state.items[index].EAN,
    //             bb:JSON.stringify(date)
                
    //         }
    //         listaCopy[index] = item;
    //         this.setState({ listaCopy })
    //         this.setState({ editButton: true, editProduct: "", editValue:"",editEan:"" })
    //         this.props.changeList(undefined, undefined, listaCopy)
           
    //   }
    changeStyle = () => {
        //listojen eri näkyvyys
        // this.setState({classname: this.state.classname === "ostoslista" ? "": "ostoslista",
        // classname1: this.state.classname1 === "ostoslistaTavarat" ? "": "ostoslistaTavarat"})
    }
    //funktio jolla voidaan tyhjentää kaikki listat
    clearAll = () => {
        this.props.changeList(1, undefined, [], [], [], [])
    }
/*  <ReactAddToCalendar buttonLabel="Lisää valittuun listaan" event={this.props.event}></ReactAddToCalendar> */
    render() {
       
        let link = "https://wa.me/?text="

        return (
            <div>
                {/* <button onClick={() => this.clearAll()}>tyhjennä kaikki</button> */}
                <div
              
                //lähetetään datechooserille valittu listan tunnus tuotelista "T" Ostoslista "O" yms...
                    onClick={() => this.props.additem(this.state.lista)}
                    style={{ alignItems:"left", backgroundColor:"grey", fontSize:18, paddingLeft:10,
                     paddingTop:6, cursor:"pointer", paddingBottom:6, boxShadow: "1px 2px 1px #757575",
                      paddingRight:10,marginTop:10, marginBottom:10,borderRadius:8, borderStyle: "solid",
                       borderWidth: 1, fontWeight:"bold", color: "white", display: "inline-block" }}>
                   Lisää valittuun listaan
                   
                </div>
                
                {/* TUOTELISTA */}
                <div style={{display: "grid", gridTemplateColumns: "repeat(2,auto)", gridGap: 2}}>
                    <DragDropContext onBeforeCapture={(id) => this.hideShow(id)} onDragEnd={this.onDragEnd}>
                        <div  style={{ width: "10.5em", borderRadius: 5,  borderWidth: this.state.lista === "T" ? 3 : 1 ,borderStyle:  "solid", borderColor: this.state.lista === "T" ? "black" : 'white'}}>
                            <h onClick={() => this.setState({ lista: "T" })} style={{ color: this.state.lista === "T" ? "black" : 'white', fontWeight: this.state.lista === "T" ? "bold" : '', fontSize: 23, paddingLeft: 10, paddingRight: 10 }}>Tuotelista</h>

                            <Droppable droppableId="droppable">
                                {(provided, snapshot) => (
                                    <div
                                        ref={provided.innerRef}
                                        style={getListStyle(snapshot.isDraggingOver)}>
                                        {this.state.items.map((item, index) => (
                                            <Draggable
                                                key={index}
                                                draggableId={item.tuote + index + "T"}
                                                index={index}>
                                                {(provided, snapshot) => (
                                                    <div
                                                        ref={provided.innerRef}
                                                        {...provided.draggableProps}
                                                        {...provided.dragHandleProps}
                                                        style={getItemStyle(
                                                            snapshot.isDragging,
                                                            provided.draggableProps.style, Date.parse(JSON.parse(item.bb))
                                                        )}>

                                                        <div style={{ minHeight: "2.5em", position: "relative", padding:2 }}>
                                                            {this.state.editProduct === index + "T" ?
                                                                <div style={{minHeight:"4.5em"}}>
                                                                    <div>
                                                                    <input className="editInput" placeholder={this.state.items[index].tuote} value={this.state.editValue} onChange={(value) => this.editItem(value, index)}></input>
                                                                    <input className="editInput" placeholder={this.state.items[index].EAN === "" ? ("viivakoodi"): this.state.items[index].EAN} value={this.state.editEan} onChange={(value) => this.editEan(value, index)} ></input>
                                                                    </div>
                                                                    <div>
                                                           
                                                                        {/* <button onClick={() => console.log(Date.parse(JSON.parse(item.bb)))}></button> */}
                                                                        <input type="date" className="editInput"  onChange={(e) => this.setState({time:e.target.value})}></input>
                                                                    {/* <input type="date" className="editInput" value="2018/07/22" onChange={(e) => this.handleDateChange(index,e.target.value)}></input> */}
                                                                   </div>
                                                                  
                                                                 {/* <div 
                                                                //   style={{position:"fixed",zIndex:9999}}
                                                                 >
                                                                     <DatePiicker  dateFormat="dd/MM/yyyy" onChange={(date) => this.handleDateChange(index,date)} selected={Date.parse(JSON.parse(this.state.items[index].bb))}></DatePicker>
                                                               </div> */}
                                                               </div>
                                                                :
                                                                item.tuote === "" ? <div className="itemDiv">{item.EAN} </div> : <div className="itemDiv"><h style={{ textDecorationLine: 'underline'}}>{item.tuote}</h>  
                                                              <div > {Intl.DateTimeFormat(['ban', 'id']).format(Date.parse(JSON.parse(this.state.items[index].bb)))}</div> 
                                                              <div> <ReactAddToCalendar 
                                                            //jokaiselle tuotteelle oma event.
                                                           
                                                            event={{
                                                                title: this.state.items[index].tuote !== "" ? this.state.items[index].tuote : this.state.items[index].EAN,
                                                                description: 'Tuotteen ' + this.state.items[index].tuote + " viimeinen käyttöpäivä." + '<a href='+link+encodeURIComponent(this.state.items[index].tuote+" menee vanhaksi " +Intl.DateTimeFormat(['ban', 'id']).format(Date.parse(JSON.parse(this.state.items[index].bb))))+'>Lähetä WhatsApp viesti!</a><br/>',
                            
                                                                startTime: JSON.parse(this.state.items[index].bb),
                                                                endTime: JSON.parse(this.state.items[index].bb),
                                                                location: 'Keittiö'
                                                            }} item={this.state.items[index].tuote} buttonLabel=""  ></ReactAddToCalendar></div>
                                                                </div>}
                                                            {this.state.editButton ? <FaEdit className="editIcon" onClick={() => this.allowEdit(index, "T")} size="20px"></FaEdit> :
                                                                this.state.editProduct === index + "T" ? <FaCheck className="editIcon" onClick={() => this.confirmEdit(index, "T")} size="20px"></FaCheck> : null}
                                                         
                                                        </div>
                                                    </div>
                                                )}
                                            </Draggable>
                                        ))}
                                        {provided.placeholder}
                                    </div>
                                )}

                            </Droppable>  
                         <div> {this.state.items.length}</div>  
                            <script>
                              
                                </script>
                        </div>
                        <div className={this.state.classname} onClick={() => this.changeStyle()} style={{ borderRadius: 5, borderWidth: this.state.lista === "O" ? 3 : 1 ,borderStyle: "solid",  borderColor: this.state.lista === "O" ? "black" : 'white' }}>
                            <h onClick={() => this.setState({ lista: "O" })} style={{ color: this.state.lista === "O" ? "black" : 'white', fontWeight: this.state.lista === "O" ? "bold" : '', fontSize: 23, paddingLeft: 10, paddingRight: 10 }}>Ostoslista</h>
                            <Ostoslista itemstyle={this.state.classname1} changeList={this.props.changeList} addToShopList={this.addToShopList} allowEdit={this.allowEdit} confirmEdit={this.confirmEdit} editEan={this.editEan} editItem={this.editItem} editEanValue={this.state.editEan} editButton={this.state.editButton} editProduct={this.state.editProduct} ostoskori={this.state.ostoskori}></Ostoslista>
  { /*                         <Viideslista itemstyle={this.state.classname1} changeList={this.props.changeList} addToShopList={this.addToShopList} allowEdit={this.allowEdit} confirmEdit={this.confirmEdit} editEan={this.editEan} editItem={this.editItem} editEanValue={this.state.editEan} editButton={this.state.editButton} editProduct={this.state.editProduct} ostoskori={this.state.viides}></Viideslista>
 */}                      </div>
 
                        {/* OSTOSLISTA */}
                        {/*
                         <div style={{ width: "100%", borderRadius: 5 }}>
                            <h style={{ fontSize: 24, paddingLeft: 10, paddingRight: 10, borderRadius: 5 }}>Ostoslista</h>
                            <Droppable droppableId="droppable2">

                                {(provided, snapshot) => (
                                    <div
                                        ref={provided.innerRef}
                                        style={getListStyle(snapshot.isDraggingOver)}>
                                        {this.state.ostoskori.map((item, index) => (
                                            <Draggable
                                                key={index}
                                                draggableId={item.tuote + index gO"}
                                                index={index}>
                                                {(provided, snapshot) => (
                                                    <div
                                                        ref={provided.innerRef}
                                                        {...provided.draggableProps}
                                                        {...provided.dragHandleProps}
                                                        style={getItemStyle(
                                                            snapshot.isDragging,
                                                            provided.draggableProps.style
                                                        )}>
                                                        <div style={{ minHeight: "2em", position: "relative" }}> {this.state.editProduct === index + "O" ?
                                                            <div>
                                                                <input placeholder={this.state.ostoskori[index].tuote} value={this.state.editValue} onChange={(value) => this.editItem(value, index)}></input>
                                                            </div>
                                                            :
                                                            item.tuote === "" ? <div style={{ width: "84%", overflow: "hidden" }}>{item.EAN} </div> : <div style={{ width: "84%", overflow: "hidden" }}> {item.tuote} </div>}
                                                            {this.state.editButton ? <FaEdit style={{ position: "absolute", top: 0, right: 0 }} onClick={() => this.allowEdit(index, "O")} size="25px"></FaEdit> :
                                                                this.state.editProduct === index + "O" ? <FaCheck onClick={() => this.confirmEdit(index, "O")} size="35px"></FaCheck> : null}
                                                        </div>

                                                    </div>
                                                )}
                                            </Draggable>
                                        ))}
                                        {provided.placeholder}
                                    </div>
                                )}
                            </Droppable>
                        </div> 
                        */}
                        {/* HÄVIKKI */}
                        <div style={{ width: "10.6em", borderRadius: 5, borderWidth: this.state.lista === "H" ? 3 : 1,borderStyle: "solid", borderColor: this.state.lista === "H" ? "black" : 'white'  }} onClick={() => this.setState({ display_waste: this.state.display_waste === 'none' ? '' : 'none' })}>
                            <h onClick={() => this.setState({ lista: "H" })} style={{ color: this.state.lista === "H" ? "black" : 'white', fontWeight: this.state.lista === "H" ? "bold" : '', fontSize: 23, paddingLeft: 10, paddingRight: 10, borderRadius: 5 }}>Hävikki</h>

                            <Droppable droppableId="droppable4">

                                {(provided, snapshot) => (
                                    <div
                                        ref={provided.innerRef}
                                        style={getListStyle(snapshot.isDraggingOver)}>
                                        {this.state.waste.map((item, index) => (
                                            <Draggable
                                                key={index}
                                                draggableId={item.tuote + index + "W"}
                                                index={index}>
                                                {(provided, snapshot) => (
                                                    <div
                                                        ref={provided.innerRef}
                                                        {...provided.draggableProps}
                                                        {...provided.dragHandleProps}
                                                        style={getItemStyle(
                                                            snapshot.isDragging,
                                                            provided.draggableProps.style, Date.parse(JSON.parse(item.bb))
                                                        )}>
                                                        <div style={{ minHeight: "2em", position: "relative", padding:2 }}>
                                                            {this.state.editProduct === index + "H" ?
                                                                <div>
                                                                    <input className="editInput" placeholder={this.state.waste[index].tuote} value={this.state.editValue} onChange={(value) => this.editItem(value, index)}></input>
                                                                    <input className="editInput" placeholder={this.state.waste[index].EAN} value={this.state.editEan} onChange={(value) => this.editEan(value, index)}></input>
                                                                </div>
                                                                :
                                                                item.tuote === "" ? <div className="itemDiv">{item.EAN} </div> : <div className="itemDiv"><h style={{marginLeft:20, textDecorationLine: 'underline'}}> {item.tuote} </h></div>}
                                                            {this.state.editButton ? <FaEdit className="editIcon" onClick={() => this.allowEdit(index, "H")} size="20px"></FaEdit> :
                                                                this.state.editProduct === index + "H" ? <FaCheck className="editIcon" onClick={() => this.confirmEdit(index, "H")} size="20px"></FaCheck> : null}
                                                        </div>
                                                    </div>
                                                )}
                                            </Draggable>
                                        ))}
                                        {provided.placeholder}
                                    </div>
                                )}
                            </Droppable>
                        </div>
                          {/* HISTORIA */}
                          <div style={{ width: "10.5em", borderRadius: 5, borderWidth: 2,borderStyle: "solid", alignItems:"center" }} onClick={() => this.setState({ display_waste: this.state.display_waste === 'none' ? '' : 'none' })}>
                            <h style={{ fontSize: 23, paddingLeft: 10, paddingRight: 10, }} onClick={() => this.setState({ history: true, editProduct: "", editButton: true })}>Historia</h>
                            {/* {<FaTrashAlt color="black"></FaTrashAlt>} */}
                            <Droppable droppableId="droppable3">

                                {(provided, snapshot) => (
                                    <div
                                        ref={provided.innerRef}
                                        style={hideShow(snapshot.isDraggingOver)}
                                    >
                                        {this.state.used.map((item, index) => (
                                            <Draggable
                                                key={index}
                                                draggableId={item.tuote + index + "E"}
                                                index={index}>
                                                {(provided, snapshot) => (
                                                    <div
                                                        ref={provided.innerRef}
                                                        {...provided.draggableProps}
                                                        {...provided.dragHandleProps}
                                                        style={getItemStyle(
                                                            snapshot.isDragging,
                                                            provided.draggableProps.style
                                                        )}>
{/*  + item.kpl + " kpl "  <FaTrashAlt style={{ position: "absolute", top: 0, left: 0 }} onClick={() => this.props.removeitem(index)} size="15px"></FaTrashAlt>   */}
                                                        {!this.state.history ? null : <div style={{minHeight: "2em", position: "relative", padding:2 }}>
                                                            {this.state.editProduct === index + "R" ? <div>
                                                                <input className="editInput" placeholder={this.state.used[index].tuote !== "" ? this.state.used[index].tuote : this.state.used[index].EAN} value={this.state.editValue} onChange={(value) => this.editItem(value, index)}></input>
                                                                <input className="editInput" placeholder={this.state.used[index].EAN} value={this.state.editEan} onChange={(value) => this.editEan(value, index)}></input>
                                                                </div>
                                                                : item.tuote === "" ? <div className="items">{item.EAN} </div> : <div className="items"><div style={{float:"left", width:18, marginTop:4, borderStyle:"solid", borderWidth:2, borderColor:"black", borderRadius:40, padding:1}}>{item.kpl}</div><h style={{marginLeft:0, left:20, top:10, textDecorationLine: 'underline', width:20}}> {item.tuote} </h></div>}
                                                            {this.state.editButton ? <div><FaEdit className="editIcon" onClick={() => this.allowEdit(index, "R")} size="18px"></FaEdit> <FaTrashAlt style={{ position: "absolute",top:18, right:4, marginTop:2, fontSize:22}} onClick={() => this.props.removeitem(index)} size="15px"></FaTrashAlt> </div> :
                                                                this.state.editProduct === index + "R" ? <FaCheck className="editIcon" onClick={() => this.confirmEdit(index, "R")} size="20px"></FaCheck> : null}
                                                        </div>}


                                                    </div>
                                                )}
                                            </Draggable>
                                        ))}
                                        {/* {provided.placeholder} */}
                                        <div>
                                        
                                        </div>
                                    </div>
                                )}
                            </Droppable>

                        </div>                        
                    
                        <a href="mailto:groceread@gmail.com"></a>
                    </DragDropContext>
                </div>
            </div>
        )
    }
}

